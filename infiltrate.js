var InfiltrateGame = function () {
    //variables here

    this.spritesheet = new Image();
    this.canvas = document.getElementById('infiltrate-canvas');
    this.context = this.canvas.getContext('2d');
    this.canvas.width = window.innerWidth;
    this.lastCanvasWidth = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.bgPosition = 0;
    this.playerPosition = window.innerWidth * 0.1;
    this.bgVelocity = 20;
    this.mapVelocity = 7;
    this.direction = 0;
    this.isPressingA = false;
    this.isPressingD = false;
    this.isPressingE = false;
    this.isRightClicking = false;
    this.isLeftClicking = false;
    this.pressedA = false;
    this.pressedD = false;
    this.lastPressedD = 0;
    this.lastPressedA = 0;
    this.PLAYER_ANIMATION_RATE = 13;

    // player stat
    this.healthBar = document.getElementById("healthbar");
    this.playerName = localStorage.getItem("username");
    this.playerHealth = 100;

    //frames stuff 
    this.lastFrameTime = 0;
    this.deltaTime = 0;
    this.fps = 0;

    // bullet stuff
    this.ammo = 24;
    this.bulletVelocity = 30;
    this.playerBulletData = [
        { left: 0.075, top: 0.38, width: 0.025, height: 0.025 },
        { left: 0.2, top: 0.38, width: 0.025, height: 0.025 }
    ];
    this.bulletsInGame = [];
    this.ammoText = document.getElementById('ammo');
    this.maxBullets = 10;
    this.playerDamage = 20;
    this.enemyDamage = 15;

    // Pause Game
    this.isPaused = false;
    this.pauseOverlay = document.getElementById('pauseOverlay');
    this.pauseMenu = document.getElementById('pauseMenu');

    // win game
    this.interactPrompt = document.getElementById("interactprompt");
    this.timeElapsed = 0;
    this.winDuration = 0;
    this.gameIsWon = false;
    this.winText = document.getElementById('winText');

    // sprite cells array

    this.PLAYER_CELLS_HEIGHT = 107;
    this.PLAYER_CELLS_WIDTH = 107;

    // store all kinds of collisions
    this.collisionsData = [];

    // solid walls
    this.collisionsData['solids'] = [
        { left: -0.1, top: 0.25, width: 0.1, height: 0.2 },
        { left: 1.4, top: 0.25, width: 0.1, height: 0.2 }
    ];

    // trigger volume data
    this.collisionsData['triggers'] = [
        { position: { left: 1.3, top: 0.25 }, width: 0.1, height: 0.2, spriteData: null },
        { position: { left: 0.925, top: 0.4 }, width: 0.05, height: 0.05, spriteData: { spritesheet: this.spritesheet, spritesheetX: 1190, spritesheetY: 862, width: 94, height: 94 } }
    ];

    // define enemy Behaviours
    this.enemy_1_behaviour = function () {
        // if alive then continue patrol
        if (this.isAlive) {
            // if player is in line of sight then stop and shoot in that direction
            if (this.playerDetected) {
                this.isMoving = false;
            }
            // else keep on patrolling 
            else {
                // timer that determines enemy movement and actions
                this.timer += infiltrateGame.deltaTime / 1000;

                // move for 2 seconds then stop and change direction
                if (Math.floor(this.timer) >= 2) {
                    this.isMoving = true;
                    if (Math.floor(this.timer) >= 4) {
                        this.timer = 0;
                        this.isMoving = false;
                        this.isTurningLeft = !this.isTurningLeft;
                    }
                }
            }
        }
    };
    // enemies
    this.enemiesInGame = [
        new Enemy({ left: 0.4, top: 0.35 }, null, { width: 0.1, height: 0.1, moveSpeed: 1.3, lineOfSight: 0.2, rateOfFire: 2, health: 100 }, this.enemy_1_behaviour),
        new Enemy({ left: 1, top: 0.35 }, null, { width: 0.1, height: 0.1, moveSpeed: 1.3, lineOfSight: 0.2, rateOfFire: 2, health: 100 }, this.enemy_1_behaviour)
    ];

    this.triggersInGame = [];

    this.playerSprites = [
        { left: 0.1, top: 0.35, spritesheetX: 0, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 123, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 243, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 343, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 443, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 553, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 673, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 793, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 903, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 1013, spritesheetY: 162, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // reverse run
        { left: 0.1, top: 0.35, spritesheetX: 1013, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 893, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 773, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 673, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 567, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 457, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 337, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 217, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 107, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 0, spritesheetY: 1915, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // idle
        { left: 0.1, top: 0.35, spritesheetX: 0, spritesheetY: 20, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 669, spritesheetY: 18, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // aiming right
        { left: 0.1, top: 0.35, spritesheetX: 5, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 100, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 205, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // aiming left
        { left: 0.1, top: 0.35, spritesheetX: 1207, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 1117, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        { left: 0.1, top: 0.35, spritesheetX: 1012, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // shooting left
        { left: 0.1, top: 0.35, spritesheetX: 893, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },
        // shooting right
        { left: 0.1, top: 0.35, spritesheetX: 324, spritesheetY: 467, width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT },


    ];

    // contains all the sprites

    this.allSprites = [];
    this.allSprites['player'] = this.playerSprites;

    this.playerSprite = new Sprite(this.spritesheet, this.allSprites['player'], this.PLAYER_ANIMATION_RATE, this.context);


}

InfiltrateGame.prototype = {
    // methods here
    initializeImages: function () {
        this.spritesheet.src = "assets/sprites/spritesheet.png";
        this.spritesheet.onload = function (e) {
            infiltrateGame.startGame();
        };
    },
    startGame: function () {
        var sprites = [];
        sprites['player'] = this.playerSprite;
        // add triggers
        for (var i = 0; i < this.collisionsData['triggers'].length; i++) {
            this.triggersInGame.push(new TriggerVolume(this.collisionsData['triggers'][i].width, this.collisionsData['triggers'][i].height, this.collisionsData['triggers'][i].position, this.collisionsData['triggers'][i].spriteData));
        }
        // define trigger methods
        this.triggersInGame[0].setAction(function () {
            infiltrateGame.interactPrompt.setAttribute('style', 'opacity: 1; z-index: 3;');
            if (infiltrateGame.isPressingE) {
                infiltrateGame.winDuration = infiltrateGame.timeElapsed;
                if (localStorage.getItem(infiltrateGame.playerName)) {
                    if (localStorage.getItem(infiltrateGame.playerName) > infiltrateGame.winDuration) {
                        localStorage.setItem(infiltrateGame.playerName, infiltrateGame.winDuration)
                    }
                } else {
                    localStorage.setItem(infiltrateGame.playerName, infiltrateGame.winDuration);
                }
                infiltrateGame.gameIsWon = true;
                infiltrateGame.isPaused = true;
                document.getElementById("retryButton").setAttribute('style', 'opacity: 1; z-index: 1;');
                document.getElementById("returnButton").setAttribute('style', 'opacity: 1; z-index: 1;');
                infiltrateGame.interactPrompt.setAttribute('style', 'opacity: 0; z-index: -1;');
                infiltrateGame.pauseOverlay.setAttribute('style', 'opacity: 0.4; z-index: 0;');
                infiltrateGame.winText.innerHTML = "Mission Accomplished<br><br><br><br>Your Score = " + infiltrateGame.winDuration + "<br><br>Your High Score = " + localStorage.getItem(infiltrateGame.playerName);
                infiltrateGame.winText.setAttribute('style', 'opacity: 1; z-index: 0;');
            }
        });
        // ammo trigger method
        this.triggersInGame[1].setAction(function () {
            if (infiltrateGame.ammo < 24) {
                infiltrateGame.ammo = 24;
                infiltrateGame.ammoText.innerHTML = "Ammo = " + infiltrateGame.ammo;
                infiltrateGame.triggersInGame.pop();
            }
        });
        // set sprites for enemies
        this.enemiesInGame[0].setSprite(new Sprite(infiltrateGame.spritesheet, infiltrateGame.playerSprites, 13, infiltrateGame.context));
        this.enemiesInGame[1].setSprite(new Sprite(infiltrateGame.spritesheet, infiltrateGame.playerSprites, 13, infiltrateGame.context));
        requestAnimationFrame(function (now) {
            infiltrateGame.animate(now, sprites);
        });
    },
    animate: function (now, sprites) {
        this.timeElapsed = now;
        if (!this.isPaused) {
            infiltrateGame.deltaTime = now - infiltrateGame.lastFrameTime;
            infiltrateGame.fps = 1000 / infiltrateGame.deltaTime;

            // check if pressing a or pressing d or both and determine the direction of player
            if (!this.isRightClicking) {
                if (this.isPressingA) {
                    if (!this.pressedA) {
                        this.playerSprite.celwlIndex = 0;
                        this.pressedA = true;
                        this.lastPressedA = now;
                    }
                    this.direction = 1;
                } else {
                    this.pressedA = false;
                }
                if (this.isPressingD) {
                    if (!this.pressedD) {
                        this.playerSprite.cellIndex = 0;
                        this.pressedD = true;
                        this.lastPressedD = now;
                    }
                    this.direction = -1;

                } else {
                    this.pressedD = false;
                }
                if (this.isPressingA && this.isPressingD) {
                    if (this.lastPressedA > this.lastPressedD) {
                        this.direction = 1;
                    }
                    if (this.lastPressedD > this.lastPressedA) {
                        this.direction = -1;
                    }
                } else if (!this.isPressingA && !this.isPressingD) {
                    this.direction = 0;
                }
            }

            //console.log(infiltrateGame.fps);

            //set the canvas size to be the window size
            infiltrateGame.canvas.width = window.innerWidth;
            infiltrateGame.canvas.height = 7.85 / 16 * window.innerWidth;
            // render everything
            infiltrateGame.draw(sprites);
            // update player UI
            infiltrateGame.updateAndCheckHealth();
            // move depending on direction
            infiltrateGame.bgPosition += infiltrateGame.deltaTime * infiltrateGame.canvas.width / 1000000 * this.bgVelocity * this.direction;

            infiltrateGame.lastFrameTime = now;
            infiltrateGame.lastCanvasWidth = infiltrateGame.canvas.width;

            this.pauseDuration = 0;

            requestAnimationFrame(function (now) {
                infiltrateGame.animate(now, sprites);
            });
        } else {
            // if paused
            this.lastFrameTime = now;
            requestAnimationFrame(function (now) {
                infiltrateGame.animate(now, sprites);
            });
        }
    },
    draw: function (sprites) {

        infiltrateGame.interactPrompt.setAttribute('style', 'opacity: 0; z-index: -1;');

        infiltrateGame.checkSolids();
        infiltrateGame.drawBackground();
        infiltrateGame.drawMap();
        infiltrateGame.drawPlayer(sprites['player']);

    },
    drawMap: function () {
        infiltrateGame.context.fillStyle = "#FF0000";
        infiltrateGame.context.translate(infiltrateGame.bgPosition * this.mapVelocity, 0);
        //infiltrateGame.context.fillRect(window.innerWidth * 0, window.innerWidth * 0.45, infiltrateGame.canvas.width * 0.4, infiltrateGame.canvas.width * 0.025);

        infiltrateGame.drawOuters();
        infiltrateGame.drawRailings();
        infiltrateGame.drawFloors();
        infiltrateGame.drawWalls();
        infiltrateGame.drawRoofs();
        infiltrateGame.drawBoxes();

        //infiltrateGame.drawSolids();

        infiltrateGame.processTriggers();

        infiltrateGame.processEnemies();

        infiltrateGame.processBullets();

        infiltrateGame.context.translate(-infiltrateGame.bgPosition * this.mapVelocity, 0);

        //infiltrateGame.context.drawImage(this.spritesheet, 0, (-window.innerWidth * 7.85 / 16) * 2.5, infiltrateGame.canvas.width * 1.95, infiltrateGame.canvas.width * 1.95);
    },
    processEnemies: function () {
        for (var i = 0; i < this.enemiesInGame.length; i++) {
            if (this.enemiesInGame[i].enemySpecs.health <= 0) {
                this.enemiesInGame.splice(i, 1);
            } else {
                this.enemiesInGame[i].execute();
            }
        }
    },
    processTriggers: function () {
        for (var i = 0; i < this.triggersInGame.length; i++) {
            this.triggersInGame[i].draw();
            this.triggersInGame[i].isColliding();
        }
    },
    drawFloors: function () {
        for (var i = 0; i < 14; i++) {
            infiltrateGame.context.drawImage(this.spritesheet, 0, 1249, 120, 30, window.innerWidth * 0.1 * i, window.innerWidth * 0.45, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1 * (1 / 4));
        }
    },
    drawRailings: function () {
        for (var i = 0; i < 4; i++) {
            infiltrateGame.context.drawImage(this.spritesheet, 1064, 868, 90, 90, window.innerWidth * (0.4 + 0.1 * i), window.innerWidth * 0.45 - window.innerWidth * 0.1, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);
        }
    },
    drawWalls: function () {
        infiltrateGame.context.drawImage(this.spritesheet, 0, 1068, 180, 180, window.innerWidth * 0, window.innerWidth * 0.25, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2);
        infiltrateGame.context.drawImage(this.spritesheet, 0, 1068, 180, 180, window.innerWidth * 0.1 * 2, window.innerWidth * 0.25, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2);

        infiltrateGame.context.drawImage(this.spritesheet, 0, 1068, 180, 180, window.innerWidth * 0.1 * 8, window.innerWidth * 0.25, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2);
        infiltrateGame.context.drawImage(this.spritesheet, 180, 1068, 180, 180, window.innerWidth * 0.1 * 10, window.innerWidth * 0.25, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2);
        infiltrateGame.context.drawImage(this.spritesheet, 0, 1068, 180, 180, window.innerWidth * 0.1 * 12, window.innerWidth * 0.25, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2);
    },

    drawRoofs: function () {
        for (var i = 0; i < 2; i++) {
            infiltrateGame.context.drawImage(this.spritesheet, 0, 1038, 180, 30, window.innerWidth * 0.1 * i * 2, window.innerWidth * 0.2175, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2 * (30 / 180));
        }
        infiltrateGame.context.drawImage(this.spritesheet, 0, 1038, 60, 30, window.innerWidth * 0.1 * 4, window.innerWidth * 0.2175, infiltrateGame.canvas.width * 0.0675, infiltrateGame.canvas.width * 0.0675 * (30 / 60));
        for (var i = 0; i < 3; i++) {
            infiltrateGame.context.drawImage(this.spritesheet, 0, 1038, 180, 30, window.innerWidth * 0.1 * (4 + i) * 2, window.innerWidth * 0.2175, infiltrateGame.canvas.width * 0.2, infiltrateGame.canvas.width * 0.2 * (30 / 180));
        }
        infiltrateGame.context.drawImage(this.spritesheet, 0, 1038, 60, 30, window.innerWidth * 0.1 * 7.333, window.innerWidth * 0.2175, infiltrateGame.canvas.width * 0.0675, infiltrateGame.canvas.width * 0.0675 * (30 / 60));
    },
    drawBoxes: function () {
        infiltrateGame.context.drawImage(this.spritesheet, 540, 828, 90, 90, window.innerWidth * 0.1 * 2, window.innerWidth * 0.35, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);
        infiltrateGame.context.drawImage(this.spritesheet, 540, 828, 90, 90, window.innerWidth * 0.1 * 10, window.innerWidth * 0.35, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);
    },
    drawBackground: function () {
        // this is for keeping the speed ratio the same when resizing the window
        // get the ratio of the last screen width then multiply it with the current screen width
        infiltrateGame.bgPosition = infiltrateGame.canvas.width * (infiltrateGame.bgPosition / infiltrateGame.lastCanvasWidth);

        infiltrateGame.context.translate(infiltrateGame.bgPosition, 0);
        infiltrateGame.context.drawImage(this.spritesheet, 0, (-window.innerWidth * 7.85 / 16) * 2.5, infiltrateGame.canvas.width * 1.95, infiltrateGame.canvas.width * 1.95);
        infiltrateGame.context.translate(-infiltrateGame.bgPosition, 0);

    },
    drawOuters: function () {
        infiltrateGame.context.fillStyle = "#081414";
        infiltrateGame.context.fillRect(window.innerWidth * -0.5, 0, infiltrateGame.canvas.width * 0.9, infiltrateGame.canvas.width * 0.51);
        infiltrateGame.context.fillRect(window.innerWidth * 0.4, window.innerWidth * 0.45, infiltrateGame.canvas.width * 0.41, infiltrateGame.canvas.width * 0.05);
        infiltrateGame.context.fillRect(window.innerWidth * 0.8, window.innerWidth * 0.45, infiltrateGame.canvas.width * 1, infiltrateGame.canvas.width * 0.05);
        infiltrateGame.context.fillRect(window.innerWidth * 0.8, 0, infiltrateGame.canvas.width * 0.41, infiltrateGame.canvas.width * 0.25);
        infiltrateGame.context.fillRect(window.innerWidth * 1.2, 0, infiltrateGame.canvas.width * 1, infiltrateGame.canvas.width * 0.5);
    },
    drawSolids: function () {
        infiltrateGame.context.fillStyle = "#00FF00";
        for (var i = 0; i < this.collisionsData['solids'].length; i++) {
            infiltrateGame.context.fillRect(window.innerWidth * this.collisionsData['solids'][i].left, window.innerWidth * this.collisionsData['solids'][i].top, window.innerWidth * this.collisionsData['solids'][i].width, window.innerWidth * this.collisionsData['solids'][i].height);
        }
    },
    checkSolids: function () {
        var collisionX = 0;
        var collisionWidth = 0;
        var playerX = window.innerWidth * this.allSprites['player'][0].left;
        var playerWidth = window.innerWidth * 0.1;
        for (var i = 0; i < this.collisionsData['solids'].length; i++) {
            collisionX = this.bgPosition * this.mapVelocity + window.innerWidth * this.collisionsData['solids'][i].left;
            collisionWidth = window.innerWidth * this.collisionsData['solids'][i].width;
            if (playerX > collisionX && playerX < (collisionX + collisionWidth)) {
                // infiltrateGame.direction = 0; need to make it stop animate
                infiltrateGame.bgPosition -= ((collisionX + collisionWidth) - playerX) / this.mapVelocity;
            } else if ((playerX + playerWidth) > collisionX && (playerX + playerWidth) < (collisionX + collisionWidth)) {
                // infiltrateGame.direction = 0; need to make it stop animate
                infiltrateGame.bgPosition -= (collisionX - (playerX + playerWidth)) / this.mapVelocity;
            }

        }
    },
    drawPlayer: function (playerSprite) {
        infiltrateGame.context.fillStyle = "#FF0000";
        //infiltrateGame.context.fillRect(window.innerWidth * 0.1, window.innerWidth * 0.35, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);

        // aiming and firing
        if (this.isRightClicking) {
            // stop the player from moving
            this.direction = 0;
            // determine direction of aim and animate
            if (this.lastPressedA > this.lastPressedD) {
                //console.log("aiming");
                playerSprite.animateFromTo(this.deltaTime, 25, 27, false);
            } else if (this.lastPressedD >= this.lastPressedA) {
                //console.log("aiming");
                playerSprite.animateFromTo(this.deltaTime, 22, 24, false);
            }
            // fire projectile
            if (this.isLeftClicking) {
                if (this.ammo > 0) {
                    //console.log("fired");
                    this.isLeftClicking = false;
                    if (this.lastPressedA > this.lastPressedD) {
                        //console.log("fired left");
                        playerSprite.draw(28);
                        // shoot projectile from left of player box
                        //infiltrateGame.context.fillRect(window.innerWidth * 0.075, window.innerWidth * 0.38, window.innerWidth * 0.025, window.innerWidth * 0.025);
                        this.bulletsInGame.push(new Bullet(this.spritesheet, this.playerBulletData[0], this.bulletVelocity, this.context, -1));
                    } else if (this.lastPressedD >= this.lastPressedA) {
                        //console.log("fired right");
                        playerSprite.draw(29);
                        // shoot projectile from right of player box
                        //infiltrateGame.context.fillRect(window.innerWidth * 0.2, window.innerWidth * 0.38, window.innerWidth * 0.025, window.innerWidth * 0.025);
                        this.bulletsInGame.push(new Bullet(this.spritesheet, this.playerBulletData[1], this.bulletVelocity, this.context, 1));
                    }
                    this.ammo--;
                    this.ammoText.innerHTML = "Ammo = " + this.ammo;
                } else {
                    console.log("no ammo");
                }
            }
        }
        // determine which way the player is going and play the right set of sprites
        else if (this.direction == -1) {
            playerSprite.animateFromTo(this.deltaTime, 0, 9, true);
        } else if (this.direction == 1) {
            playerSprite.animateFromTo(this.deltaTime, 10, 19, true);
        } else if (this.direction == 0) {
            if (this.lastPressedA > this.lastPressedD) {
                playerSprite.draw(20);
            } else if (this.lastPressedD >= this.lastPressedA) {
                playerSprite.draw(21);
            }
        }


    },
    processBullets: function () {
        if (this.bulletsInGame.length > 0) {
            //console.log("rendering bullets");
            for (var i = 0; i < this.bulletsInGame.length; i++) {

                this.bulletsInGame[i].draw(this.bulletsInGame[i].direction);
                if (this.bulletsInGame[i].checkCollideWithWall() || this.bulletsInGame[i].checkCollideWithEnemy() || this.bulletsInGame[i].checkCollideWithPlayer()) {
                    this.bulletsInGame.splice(i, 1);
                }
                //this.bulletsInGame[i].checkCollideWithEnemy();
            }
        }
        // limit the number of bullets in the game
        if (this.bulletsInGame.length > this.maxBullets) {
            this.bulletsInGame.shift();
        }
    },
    updateAndCheckHealth: function () {
        //console.log(this.playerHealth);
        infiltrateGame.healthBar.setAttribute('style', "width: " + this.playerHealth + "%;");
        if (this.playerHealth <= 0) {
            infiltrateGame.healthBar.setAttribute('style', "width: 0%;");
            infiltrateGame.gameOver();
        }
    },
    gameOver: function () {
        infiltrateGame.interactPrompt.setAttribute('style', 'opacity: 0; z-index: -1;');
        infiltrateGame.isPaused = true;
        document.getElementById("retryButton").setAttribute('style', 'opacity: 1; z-index: 1;');
        document.getElementById("returnButton").setAttribute('style', 'opacity: 1; z-index: 1;');
        infiltrateGame.pauseOverlay.setAttribute('style', 'opacity: 0.4; z-index: 0;');
        infiltrateGame.winText.innerHTML = "YOU DIED";
        infiltrateGame.winText.setAttribute('style', 'opacity: 1; z-index: 0;');
    }
}

var infiltrateGame = new InfiltrateGame();

infiltrateGame.initializeImages();

window.addEventListener("keydown", function (event) {
    if (event.isComposing || event.keyCode === 68) {
        infiltrateGame.isPressingD = true;
    }

    if (event.isComposing || event.keyCode === 65) {
        infiltrateGame.isPressingA = true;
    }

    if (event.isComposing || event.keyCode === 69) {
        infiltrateGame.isPressingE = true;
    }
});
window.addEventListener("keyup", function (event) {
    if (event.keyCode === 68) {
        infiltrateGame.isPressingD = false;
    }

    if (event.keyCode === 65) {
        infiltrateGame.isPressingA = false;
    }

    if (event.keyCode === 27) {
        if (!infiltrateGame.isPaused) {
            pauseGame();
        } else {
            resumeGame();
        }
    }

    if (event.keyCode === 69) {
        infiltrateGame.isPressingE = false;
    }
});
window.onblur = function (e) {
    pauseGame();
};
function mouseDown(event) {
    if (event.button == 2) {
        //console.log("holding right mouse down");
        infiltrateGame.playerSprite.cellIndex = 0;
        infiltrateGame.isRightClicking = true;
    }
    if (event.button == 0) {
        //console.log("holding left mouse down");
        infiltrateGame.isLeftClicking = true;
    }
}
function mouseUp(event) {
    if (event.button == 2) {
        //console.log("lifted right mouse");
        infiltrateGame.isRightClicking = false;
    }
    if (event.button == 0) {
        //console.log("lifted right mouse");
        infiltrateGame.isLeftClicking = false;
    }
}
function pauseGame() {
    if (!infiltrateGame.gameIsWon) {
        infiltrateGame.pauseOverlay.setAttribute('style', 'opacity: 0.4; z-index: 0;');
        infiltrateGame.pauseMenu.setAttribute('style', 'opacity: 0.9; z-index: 0;');
        infiltrateGame.isPaused = true;
    }
}
function resumeGame() {
    if (!infiltrateGame.gameIsWon) {
        infiltrateGame.pauseOverlay.setAttribute('style', 'opacity: 0; z-index: -1;');
        infiltrateGame.pauseMenu.setAttribute('style', 'opacity: 0; z-index: -1;');
        infiltrateGame.isPaused = false;
    }

}

