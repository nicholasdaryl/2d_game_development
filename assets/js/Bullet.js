var Bullet = function (spritesheet, bulletData, bulletVelocity, context, direction) {
    this.spritesheet = spritesheet;
    this.bulletData = bulletData;
    this.bulletVelocity = bulletVelocity;
    this.context = context;
    this.bulletPosition = -infiltrateGame.bgPosition * infiltrateGame.mapVelocity;
    //console.log("infiltrateGame.bgPosition : " + infiltrateGame.bgPosition);
    this.direction = direction;
    // this.hitSprite = new Sprite(spritesheet, cellsData, 2.5, context);

}
Bullet.prototype = {
    draw: function (direction) {
        // still speed isnt proper relative to the game world, just relative to the screen  
        this.bulletPosition += infiltrateGame.deltaTime * window.innerWidth / 10000 * this.bulletVelocity * direction;
        this.context.translate(window.innerWidth * this.bulletData.left + this.bulletPosition, window.innerWidth * this.bulletData.top);

        this.context.drawImage(this.spritesheet, 495, 499, 33, 33, 0, 0, window.innerWidth * this.bulletData.width, window.innerWidth * this.bulletData.height);

        this.context.translate(-window.innerWidth * this.bulletData.left - this.bulletPosition, -window.innerWidth * this.bulletData.top);
    },
    checkCollideWithWall: function () {
        var collisionX = 0;
        var collisionWidth = 0;
        var bulletX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * this.bulletData.left + this.bulletPosition;
        //console.log(bulletX);
        var bulletWidth = window.innerWidth * this.bulletData.width;

        for (var i = 0; i < infiltrateGame.collisionsData['solids'].length; i++) {
            collisionX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * infiltrateGame.collisionsData['solids'][i].left;
            collisionWidth = window.innerWidth * infiltrateGame.collisionsData['solids'][i].width;
            if (bulletX > collisionX && bulletX < (collisionX + collisionWidth)) {
                // destroy bullet and draw flash sprite
                //console.log("bullet hit a wall right side");

                return true;
            } else if ((bulletX + bulletWidth) > collisionX && (bulletX + bulletWidth) < (collisionX + collisionWidth)) {
                // destroy bullet and draw flash sprite
                //console.log("bullet hit a wall left side");
                return true;
            }
        }
    },
    checkCollideWithEnemy: function () {
        var bulletX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * this.bulletData.left + this.bulletPosition;
        var bulletXend = bulletX + window.innerWidth * this.bulletData.width;
        var enemyX = 0;
        var enemyXend = 0;
        for (var i = 0; i < infiltrateGame.enemiesInGame.length; i++) {
            enemyX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * infiltrateGame.enemiesInGame[i].position.left + infiltrateGame.enemiesInGame[i].offset;
            enemyXend = enemyX + window.innerWidth * infiltrateGame.enemiesInGame[i].enemySpecs.width;
            if (bulletX >= enemyX && bulletX <= enemyXend) {
                infiltrateGame.enemiesInGame[i].enemySpecs.health -= infiltrateGame.playerDamage;
                return true;
            } else if (bulletXend >= enemyX && bulletXend <= enemyXend) {
                infiltrateGame.enemiesInGame[i].enemySpecs.health -= infiltrateGame.playerDamage;
                return true;
            }
        }
    },
    checkCollideWithPlayer: function () {
        var bulletX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * this.bulletData.left + this.bulletPosition;
        var bulletXend = bulletX + window.innerWidth * this.bulletData.width;
        var playerX = window.innerWidth * infiltrateGame.playerSprites[0].left;
        var playerXend = playerX + window.innerWidth * 0.1;

        if (bulletX >= playerX && bulletX <= playerXend) {
            infiltrateGame.playerHealth -= infiltrateGame.enemyDamage;
            return true;
        } else if (bulletXend >= playerX && bulletXend <= playerXend) {
            infiltrateGame.playerHealth -= infiltrateGame.enemyDamage;
            return true;
        }

    }
}

