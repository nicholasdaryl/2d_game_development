var Sprite = function (spritesheet, cellsData, animationRate, context) {
    this.spritesheet = spritesheet;
    this.cellsData = cellsData;
    this.animationRate = animationRate;
    this.context = context;
    this.cellIndex = 0;
}
Sprite.prototype = {
    draw: function (cellIndex) {
        this.context.drawImage(this.spritesheet, this.cellsData[cellIndex].spritesheetX, this.cellsData[cellIndex].spritesheetY, this.cellsData[cellIndex].width, this.cellsData[cellIndex].height, window.innerWidth * this.cellsData[cellIndex].left, window.innerWidth * this.cellsData[cellIndex].top, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);
    },
    animate: function (deltaTime) {
        this.cellIndex += deltaTime / 1000 * this.animationRate;

        if (Math.floor(this.cellIndex) > this.cellsData.length - 1) {
            this.cellIndex = 0;
        }
        //console.log(Math.floor(this.cellIndex));
        this.context.drawImage(this.spritesheet, this.cellsData[Math.floor(this.cellIndex)].spritesheetX, this.cellsData[Math.floor(this.cellIndex)].spritesheetY, this.cellsData[Math.floor(this.cellIndex)].width, this.cellsData[Math.floor(this.cellIndex)].height, window.innerWidth * this.cellsData[Math.floor(this.cellIndex)].left, window.innerWidth * this.cellsData[Math.floor(this.cellIndex)].top, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);

        if (Math.floor(this.cellIndex) > this.cellsData.length - 1) {
            this.cellIndex = 0;
        }
    },
    animateFromTo: function (deltaTime, startIndex, endIndex, loop) {
        this.cellIndex += deltaTime / 1000 * this.animationRate;
        if (startIndex + Math.floor(this.cellIndex) > endIndex) {
            if (loop) {
                this.cellIndex = 0;
            } else {
                this.cellIndex = endIndex - startIndex;
            }
        }
        //console.log(Math.floor(this.cellIndex));
        this.context.drawImage(this.spritesheet, this.cellsData[startIndex + Math.floor(this.cellIndex)].spritesheetX, this.cellsData[startIndex + Math.floor(this.cellIndex)].spritesheetY, this.cellsData[startIndex + Math.floor(this.cellIndex)].width, this.cellsData[startIndex + Math.floor(this.cellIndex)].height, window.innerWidth * this.cellsData[startIndex + Math.floor(this.cellIndex)].left, window.innerWidth * this.cellsData[startIndex + Math.floor(this.cellIndex)].top, infiltrateGame.canvas.width * 0.1, infiltrateGame.canvas.width * 0.1);

        if (startIndex + Math.floor(this.cellIndex) > endIndex) {
            if (loop) {
                this.cellIndex = 0;
            } else {
                this.cellIndex = endIndex - startIndex;
            }
        }
    }
}