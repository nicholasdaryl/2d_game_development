/*
    TODO:
    - make enemy
    - make game over
    - put sounds
    - git
    - changing difficulty
*/


//npc class can move left right and have line of sight can shoot bullets at rate

var Enemy = function (position, sprite, enemySpecs, behaviour) {
    this.position = position;
    this.isMoving = false;
    this.enemySpecs = enemySpecs;
    this.behaviour = behaviour;
    this.timer = 0;
    this.fireRateTimer = 0;
    this.playerDetected = false;
    this.isTurningLeft = false;
    this.isFacingLeft = false;
    this.health = enemySpecs.health;
    this.damage = enemySpecs.damage;
    this.isAlive = true;
    this.offset = 0;
    this.sprite = null;
}

Enemy.prototype = {
    execute: function () {
        this.draw();
        this.checkMovement();
        this.behaviour();
        this.checkLineOfSight();
    },
    setSprite: function (sprite) {
        this.sprite = sprite;
    },
    draw: function () {
        //console.log(this.enemySpecs.health);
        // this is to deal with the offset ratio when resizing the window
        this.offset = window.innerWidth * (this.offset / infiltrateGame.lastCanvasWidth);

        infiltrateGame.context.translate(this.offset, 0);

        infiltrateGame.context.fillStyle = "#E4E4E4";
        //infiltrateGame.context.fillRect(window.innerWidth * this.position.left, window.innerWidth * this.position.top, window.innerWidth * this.enemySpecs.width, window.innerWidth * this.enemySpecs.height);

        infiltrateGame.context.fillStyle = "#FF0000";

        // animations

        // if walking right
        if (!this.playerDetected) {
            if (this.isMoving && !this.isFacingLeft) {
                infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                this.sprite.animateFromTo(infiltrateGame.deltaTime, 0, 9, true);
                infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
            } else if (this.isMoving && this.isFacingLeft) {
                infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                this.sprite.animateFromTo(infiltrateGame.deltaTime, 10, 19, true);
                infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
            } else {
                if (this.isFacingLeft) {
                    infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                    this.sprite.draw(20);
                    infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                } else {
                    infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                    this.sprite.draw(21);
                    infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
                }
            }
        } else {
            this.aim();
        }

        // draw line of sight according to the direction facing

        if (!this.isFacingLeft) {
            infiltrateGame.context.translate(window.innerWidth * this.position.left, 0);
            //infiltrateGame.context.fillRect(window.innerWidth * this.enemySpecs.width, window.innerWidth * this.position.top, window.innerWidth * this.enemySpecs.lineOfSight, window.innerWidth * this.enemySpecs.height);

            infiltrateGame.context.translate(-this.offset - window.innerWidth * this.position.left, 0);
        } else {
            infiltrateGame.context.translate((window.innerWidth * this.position.left - (window.innerWidth * this.enemySpecs.lineOfSight)), 0);
            //infiltrateGame.context.fillRect(0, window.innerWidth * this.position.top, window.innerWidth * this.enemySpecs.lineOfSight, window.innerWidth * this.enemySpecs.height);

            infiltrateGame.context.translate(-this.offset - (window.innerWidth * this.position.left - (window.innerWidth * this.enemySpecs.lineOfSight)), 0);
        }
    },
    moveRight: function () {
        this.isFacingLeft = false;

        this.offset += infiltrateGame.deltaTime * this.enemySpecs.moveSpeed * window.innerWidth / 10000;
    },
    moveLeft: function () {
        this.isFacingLeft = true;

        this.offset -= infiltrateGame.deltaTime * this.enemySpecs.moveSpeed * window.innerWidth / 10000;
    },
    checkLineOfSight: function () {
        var playerX = window.innerWidth * infiltrateGame.playerSprites[0].left;
        var playerXend = playerX + window.innerWidth * 0.1;
        var sightX;
        if (!this.isFacingLeft) {
            sightX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + (window.innerWidth * this.position.left + window.innerWidth * this.enemySpecs.width) + this.offset;
        } else {
            sightX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + (window.innerWidth * this.position.left - window.innerWidth * this.enemySpecs.lineOfSight) + this.offset;
        }
        var sightXend = sightX + window.innerWidth * this.enemySpecs.lineOfSight;

        if (playerX > sightX && playerX < sightXend) {
            this.playerDetected = true;
            this.fire();
            //console.log("within line of sight");
        } else if (playerXend > sightX && playerXend < sightXend) {
            this.playerDetected = true;
            this.fire();
            //console.log("within line of sight");
        } else {
            this.playerDetected = false;
        }
    },
    checkMovement: function () {
        if (this.isTurningLeft && this.isMoving) {
            this.moveLeft();
        } else if (!this.isTurningLeft && this.isMoving) {
            this.moveRight();
        }
    },
    fire: function () {
        // determine the bullet spawn position
        var startingBulletPosition;
        if (this.isFacingLeft) {
            startingBulletPosition = (infiltrateGame.bgPosition * infiltrateGame.mapVelocity + (window.innerWidth * this.position.left) + this.offset) / window.innerWidth;
        } else {
            startingBulletPosition = (infiltrateGame.bgPosition * infiltrateGame.mapVelocity + (window.innerWidth * this.position.left + window.innerWidth * this.enemySpecs.width) + this.offset) / window.innerWidth;
        }
        this.fireRateTimer += infiltrateGame.deltaTime / 1000 * this.enemySpecs.rateOfFire;
        // fire depending on fire rate
        if (this.fireRateTimer >= 1) {
            // fire depending on direction facing towards
            if (!this.isFacingLeft) {
                infiltrateGame.bulletsInGame.push(new Bullet(infiltrateGame.spritesheet, { left: startingBulletPosition, top: 0.38, width: 0.025, height: 0.025 }, infiltrateGame.bulletVelocity, infiltrateGame.context, 1));
            } else {
                infiltrateGame.bulletsInGame.push(new Bullet(infiltrateGame.spritesheet, { left: startingBulletPosition, top: 0.38, width: 0.025, height: 0.025 }, infiltrateGame.bulletVelocity, infiltrateGame.context, -1));
            }
            this.fireRateTimer = 0;
        }
    },
    aim: function () {
        if (this.isFacingLeft) {
            infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
            this.sprite.animateFromTo(infiltrateGame.deltaTime, 25, 27, false);
            infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
        } else {
            infiltrateGame.context.translate((window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
            this.sprite.animateFromTo(infiltrateGame.deltaTime, 22, 24, false);
            infiltrateGame.context.translate(-(window.innerWidth * this.position.left - window.innerWidth * 0.1), 0);
        }
    }
}

