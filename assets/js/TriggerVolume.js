var TriggerVolume = function (width, height, position, spriteData) {
    this.width = width;
    this.height = height;
    this.position = position;
    this.spriteData = spriteData;
    this.action = null;
}

TriggerVolume.prototype = {
    draw: function () {
        if (this.spriteData) {
            infiltrateGame.context.drawImage(this.spriteData.spritesheet, this.spriteData.spritesheetX, this.spriteData.spritesheetY, this.spriteData.width, this.spriteData.height, window.innerWidth * this.position.left, window.innerWidth * this.position.top, window.innerWidth * this.width, window.innerWidth * this.height);
        } else {
            // infiltrateGame.context.fillStyle = "#E4E4E4";
            // infiltrateGame.context.fillRect(window.innerWidth * this.position.left, window.innerWidth * this.position.top, window.innerWidth * this.width, window.innerWidth * this.height);
        }
    },
    isColliding: function () {
        var playerX = window.innerWidth * infiltrateGame.playerSprites[0].left;
        var playerXend = playerX + window.innerWidth * 0.1;
        var collisionX = infiltrateGame.bgPosition * infiltrateGame.mapVelocity + window.innerWidth * this.position.left;
        var collisionXend = collisionX + window.innerWidth * this.width;
        // console.log("cosition X: " + collisionX);
        if (playerX > collisionX && playerX < collisionXend) {
            if (this.action != null) {
                this.action();
            }
            return true;
        } else if (playerXend > collisionX && playerXend < collisionXend) {
            if (this.action != null) {
                this.action();
            }
            return true;
        }
    },
    setAction: function (action) {
        this.action = action;
    }
}